import PropTypes from 'prop-types';
// import ModalWrapper from '../ModalWrapper/ModalWrapper';

export default function Modal ({children, classNames}) {
    return (
        <div className={`${classNames} modal`}>
            {children}
        </div>
    )
}

Modal.propTypes = {
    children: PropTypes.node.isRequired,
    classNames: PropTypes.string,
    onClose: PropTypes.func,
    isVisible: PropTypes.bool.isRequired
}