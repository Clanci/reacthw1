import { useState } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal/Modal';
import ModalBody from './components/Modal/ModalBody/ModalBody';
import ModalClose from './components/Modal/ModalClose/ModalClose';
import ModalFooter from './components/Modal/ModalFooter/ModalFooter';
import ModalHeader from './components/Modal/ModalHeader/ModalHeader';
import ModalWrapper from './components/Modal/ModalWrapper/ModalWrapper';
import imageSrc from './assets/image.png';

function App() {
  const [firstModalVisible, setFirstModalVisible] = useState(false);
  const [secondModalVisible, setSecondModalVisible] = useState(false);
  

  const toggleFirstModal = () => {
    setFirstModalVisible(prevState => !prevState);
  };
  const toggleSecondModal = () => {
    setSecondModalVisible(prevState => !prevState);
  };
  const addToFavorite = ()=>{
    console.log("add to favorite")
  }
  const deleteProduct = () =>{
    console.log("delete")
  }

  return (
    <>
      {!firstModalVisible && !secondModalVisible && (
        <Button onClick={toggleFirstModal} type={"button"} classNames={"firstModal"}>
          Open first modal
        </Button>
      )}
      {!secondModalVisible && !firstModalVisible && (
        <Button onClick={toggleSecondModal} type={"button"} classNames={"secondModal"}>
          Open second modal
        </Button>
      )}
      
      {firstModalVisible && (
        <Modal
          classNames={"firstModal"} onClose={toggleFirstModal} isVisible={firstModalVisible}
        >
          <ModalWrapper>
            <ModalClose onClick={toggleFirstModal}/>
            <ModalHeader>
              <img src={imageSrc} alt="#" />
              <h2>Product Delete!</h2>
            </ModalHeader>
            <ModalBody>
              <p> By clicking the “Yes, Delete” button, Clancy will be deleted.</p>
            </ModalBody>
            <ModalFooter
              firstText={"no, cancel"}
              firstClick={toggleFirstModal}
              secondaryText={"yes, delete"}
              secondaryClick={deleteProduct}
            >
            </ModalFooter>
          </ModalWrapper>          
        </Modal>
      )}

      {secondModalVisible &&(
        <Modal classNames={"secondModal"} onClose={toggleSecondModal} isVisible={secondModalVisible}>
          <ModalWrapper>
            <ModalClose onClick={toggleSecondModal}/>
            <ModalHeader>
              <h2>Clancy</h2> 
            </ModalHeader>
            <ModalBody>
              <p> If you can`t see, I am Clancy</p>
            </ModalBody>
            <ModalFooter
              firstText={"add to favorite"}
              firstClick={addToFavorite}
            >
            </ModalFooter>
          </ModalWrapper>
        </Modal>
      )}
    </>
  )
}

export default App